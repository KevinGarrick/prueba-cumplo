PACKAGE_NAME=prueba-cumplo
VERSION=0.0.1
HOST_PORT=8000
PROJECT_FOLDER=.
SERVER_MODULE=${PACKAGE_NAME}.server
GIT_DIR=$(shell pwd)

clean-pyc:
	rm -Rf tests/__pycache__
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	find . -name '*~' -delete

clean-build:
	rm -Rf build/
	rm -Rf dist/
	rm -Rf *.egg-info
	rm -Rf .cache/

clean: clean-pyc clean-build

launch-script:
	FLASK_APP=server.py FLASK_DEBUG=1 python3.7 -m flask run

launch:
	FLASK_APP=server.py FLASK_DEBUG=1 python3.7 -m flask run --host=0.0.0.0 --port=$(HOST_PORT)

build: clean
	docker build -t $(PACKAGE_NAME) $(GIT_DIR)

shell: clean
	docker run -it --rm -v $(GIT_DIR):/app-run -p $(HOST_PORT):$(HOST_PORT) -w /app-run/$(PROJECT_FOLDER) --entrypoint=/bin/ash $(PACKAGE_NAME) 

install-requirements:
	pip -v install -r requirements.txt
