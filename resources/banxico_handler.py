import os
import requests
import logging

sh = logging.StreamHandler()
logging.basicConfig(format='%(asctime)s |%(name)s|%(levelname)s|%(message)s',
                    level=logging.INFO,
                    handlers=[sh])

TOKEN_SIE = "6b63ac9cd6a5292a89a778343ca0d1d4d33ab6763f1fd0bd863ec802edaff9f5"
HOST = "https://www.banxico.org.mx/SieAPIRest/service/v1/series"


class Handler(requests.Session):
    def __init__(self, api_host):
        super().__init__()
        self.api_host = api_host

    def request(self, method, url, **kwargs):
        logging.info(f"Request: {method} {self.api_host}{url}")
        return super().request(method, self.api_host + url, **kwargs)

    
    
handler = Handler(HOST)