FROM python:3.7-alpine3.9

RUN pip install gunicorn\
    && apk add --update build-base git make\
    && apk add --no-cache tzdata\
    && apk add libffi-dev\
    && cp /usr/share/zoneinfo/America/Mexico_City /etc/localtime \
    && echo "America/Mexico_City" > /etc/timezone

# RUN apk add wkhtmltopdf xvfb
WORKDIR /app-run
COPY . /app-run

RUN make install-requirements

ENTRYPOINT ["gunicorn", "-w 4", "--bind", "0.0.0.0:8000", "--access-logfile", "-", "--error-logfile", "-", "server:app"]
