from datetime import date
from flask.views import MethodView
from flask import request, jsonify, abort
from http import HTTPStatus
from .banxico_handler import handler
TOKEN_SIE = "6b63ac9cd6a5292a89a778343ca0d1d4d33ab6763f1fd0bd863ec802edaff9f5"
UDIS = "SP68257"
DLLS = "SF43718"


def get_statistics(values):

    max_value = max(values, key=lambda x: float(x["dato"]))
    min_value = min(values, key=lambda x: float(x["dato"]))

    summation = 0
    for i in [float(x["dato"]) for x in values]:
        summation += i
    avg = summation/len(values)

    return {
        "min": min_value,
        "max": max_value,
        "avg": avg
    }


class BanxicoView(MethodView):

    def get(self):
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        if not start_date or not end_date:
            abort(HTTPStatus.BAD_REQUEST)

        resource = f"/{DLLS},{UDIS}/datos/{start_date}/{end_date}?token={TOKEN_SIE}"
        banxico_response = handler.get(resource)
        data_banxico = banxico_response.json()
        series = []
        for serie in data_banxico.get("bmx", {}).get("series", []):
            values= serie.get("datos")
            print(serie)
            statistics = {}
            if values:
                statistics = get_statistics(values)
            series.append(
                {
                    "data": values,
                    "statistics": statistics,
                    "title": serie["titulo"],
                    "type": "udis" if serie["idSerie"] == UDIS else "dlls"
                })

        return jsonify(series=series), HTTPStatus.OK


class TiieListView(MethodView):

    def get(self):
        resource = f"/SF331451,SF43783,SF43878,SF111916,SF43947/datos/oportuno?token={TOKEN_SIE}"
        banxico_response = handler.get(resource)
        data_banxico = banxico_response.json()
        return jsonify(series=data_banxico.get("bmx").get("series")), HTTPStatus.OK


class TiieItemView(MethodView):

    def get(self, serie_id):
        resource = f"/{serie_id}/datos?token={TOKEN_SIE}"
        banxico_response = handler.get(resource)
        data_banxico = banxico_response.json()
        return jsonify(data_banxico.get("bmx").get("series")[0]), HTTPStatus.OK
