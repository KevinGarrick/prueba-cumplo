import os
from flask import (Flask, request, jsonify)
from resources.banxico import BanxicoView, TiieItemView, TiieListView
from flask_cors import CORS

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app, resources=r"/*", headers="Content-Type")


@app.route('/', methods=['GET'])
def health():
    return jsonify(status='ok')


banxico_view = BanxicoView.as_view('banxico_api')
app.add_url_rule('/dlls_n_udis',
                 view_func=banxico_view,
                 methods=['GET'])

tiie_list_view = TiieListView.as_view('tiie_list_api')
app.add_url_rule('/tiies',
                 view_func=tiie_list_view,
                 methods=['GET'])

tiie_item_view = TiieItemView.as_view('tiie_item_api')
app.add_url_rule('/tiies/<string:serie_id>',
                 view_func=tiie_item_view,
                 methods=['GET'])
